/*
 * Copyright 2004 - 2013 Wayne Grant
 *           2013 - 2018 Kai Kramer
 *
 * This file is part of KeyStore Explorer.
 *
 * KeyStore Explorer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * KeyStore Explorer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KeyStore Explorer.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.kse;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.kse.crypto.CryptoException;
import org.kse.crypto.x509.KseX500NameStyle;
import org.kse.version.Version;
import org.quijotelui.Historia;
import org.quijotelui.HistoriaCertificado;

import java.io.File;
import java.security.Security;
import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * Main class to start the KeyStore Explorer (QuijoteLuiKS) application.
 *
 */
public class QuijoteLuiKS {
	private static ResourceBundle props = ResourceBundle.getBundle("org/kse/version");

	static {
		// set default style for Bouncy Castle's X500Name class
		X500Name.setDefaultStyle(KseX500NameStyle.INSTANCE);

		// we start with system proxy settings and switch later depending on preferences
		System.setProperty("java.net.useSystemProxies", "true");

		// allow lax parsing of malformed ASN.1 integers
		System.setProperty("org.bouncycastle.asn1.allow_unsafe_integer", "true");
	}


	/**
	 * Start the QuijoteLuiKS application.
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) throws CryptoException {
			initialiseSecurity();
			System.out.println("QuijoteLuiKS");
			informacion();
	}

	private static void initialiseSecurity()  {

		// Add BouncyCastle provider
		Security.addProvider(new BouncyCastleProvider());
	}

	/**
	 * Get application name.
	 *
	 * @return Application name
	 */
	public static String getApplicationName() {
		return props.getString("KSE.Name");
	}

	/**
	 * Get application version.
	 *
	 * @return Application version
	 */
	public static Version getApplicationVersion() {
		return new Version(props.getString("KSE.Version"));
	}

	/**
	 * Get full application name, ie Name and Version.
	 *
	 * @return Full application name
	 */
	public static String getFullApplicationName() {
		return MessageFormat.format(props.getString("KSE.FullName"), QuijoteLuiKS.getApplicationName(),
				QuijoteLuiKS.getApplicationVersion());
	}

	static void informacion() throws CryptoException {
		HistoriaCertificado history =  new HistoriaCertificado();
		Scanner scanner = new Scanner(System.in);
		String PKCS12_RESOURCE = "/data/BCE";
		String PKCS12_PASSWORD = "";

		System.out.println("Ingrese el nombre del archivo P12: ");
		String nombreP12 = scanner.next();
		PKCS12_RESOURCE = PKCS12_RESOURCE + File.separatorChar + nombreP12;
		System.out.println("Archivo Firma P12: " + PKCS12_RESOURCE);

		System.out.println("Ingrese la contraseña: ");
		PKCS12_PASSWORD = scanner.next();

		List<Historia> historias = history.getInformacion(PKCS12_RESOURCE, PKCS12_PASSWORD);

		for (Historia historia : historias) {
			System.out.println(historia.getOwnerCert());
			System.out.println(historia.getIssuerCert());
			System.out.println(historia.getFormatDate());
			System.out.println(historia.getDateExpiry());
			System.out.println(historia.getDaysToExpiry());
		}
	}
}
