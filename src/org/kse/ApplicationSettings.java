/*
 * Copyright 2004 - 2013 Wayne Grant
 *           2013 - 2018 Kai Kramer
 *
 * This file is part of KeyStore Explorer.
 *
 * KeyStore Explorer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * KeyStore Explorer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KeyStore Explorer.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.kse;

import org.kse.crypto.digest.DigestType;
import org.kse.crypto.keypair.KeyPairType;
import org.kse.crypto.secretkey.SecretKeyType;
import org.kse.utilities.StringUtils;
import org.kse.utilities.net.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.ProxySelector;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
/**
 * QuijoteLuiKS Application settings. Load, save and provide access to the various
 * application settings. Settings persist to Java preferences.
 *
 */
public class ApplicationSettings {


	private static final String PREFS_NODE = "/org/kse";
	private static final String PREFS_NODE_OLD = "/net/sf/keystore_explorer";

	private static final String KSE3_PROXY = "kse3.proxy";
	private static final String KSE3_SOCKSPORT = "kse3.socksport";
	private static final String KSE3_SOCKSHOST = "kse3.sockshost";
	private static final String KSE3_HTTPSPORT = "kse3.httpsport";
	private static final String KSE3_HTTPSHOST = "kse3.httpshost";
	private static final String KSE3_HTTPPORT = "kse3.httpport";
	private static final String KSE3_HTTPHOST = "kse3.httphost";
	private static final String KSE3_PACURL = "kse3.pacurl";

	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static ApplicationSettings applicationSettings;
	private boolean useCaCertificates;
	private File caCertificatesFile;
	private boolean useWindowsTrustedRootCertificates;
	private boolean enableImportTrustedCertTrustCheck;
	private boolean enableImportCaReplyTrustCheck;
	private KeyPairType generateKeyPairType;
	private int generateKeyPairSize;
	private SecretKeyType generateSecretKeyType;
	private int generateSecretKeySize;
	private DigestType certificateFingerprintType;
	private Rectangle sizeAndPosition;
	private boolean showToolBar;
	private boolean showStatusBar;
	private int tabLayout;
	private File[] recentFiles;
	private File currentDirectory;
	private String lookAndFeelClass;
	private boolean lookAndFeelDecorated;
	private boolean showTipsOnStartUp;
	private int nextTipIndex;
	private String defaultDN;
	private String sslHosts;
	private String sslPorts;
	private boolean autoUpdateCheckEnabled;
	private Date autoUpdateCheckLastCheck;
	private int autoUpdateCheckInterval;
	private String p11Libs;
	private String language;
	private int expiryWarnDays;

	private ApplicationSettings() {

		// one-time conversion from old to new preferences location:
		Preferences root = Preferences.userRoot();
		try {
			// if preferences exist under /net/sf/keystore_explorer but not under /org/kse ...
			if (root.nodeExists(PREFS_NODE_OLD) && !root.nodeExists(PREFS_NODE)) {

				// ... then copy settings from old to new subtree
				Preferences prefsOld = root.node(PREFS_NODE_OLD);
				Preferences prefsNew = root.node(PREFS_NODE);

				for (String key : prefsOld.keys()) {
					prefsNew.put(key, prefsOld.get(key, ""));
				}

				prefsNew.flush();
			}
		} catch (BackingStoreException e) {
			// ignore errors here
		}

	}

	/**
	 * Get singleton instance of application settings. If first call the
	 * application settings are loaded.
	 *
	 * @return Application settings
	 */
	public static synchronized ApplicationSettings getInstance() {
		if (applicationSettings == null) {
			applicationSettings = new ApplicationSettings();
		}

		return applicationSettings;
	}

	/**
	 * Load application settings from persistent store.
	 */
	private File cleanFilePath(File filePath) {
		try {
			return filePath.getCanonicalFile();
		} catch (IOException e) {
			return filePath;
		}
	}

	private Date getDate(Preferences preferences, String name,  Date def) {
		try {
			return dateFormat.parse(preferences.get(name, dateFormat.format(def)));
		} catch (ParseException e) {
			return def;
		}
	}

	/**
	 * Save application settings to persistent store.
	 */

	private void getCurrentProxySettings(Preferences preferences) {
		// Get current proxy settings
		ProxySelector proxySelector = ProxySelector.getDefault();

		if (proxySelector instanceof NoProxySelector) {
			preferences.put(KSE3_PROXY, ProxyConfigurationType.NONE.name());
		} else if (proxySelector instanceof SystemProxySelector) {
			preferences.put(KSE3_PROXY, ProxyConfigurationType.SYSTEM.name());
		}else if (proxySelector instanceof PacProxySelector) {
			PacProxySelector pacProxySelector = (PacProxySelector) proxySelector;

			preferences.put(KSE3_PACURL, pacProxySelector.getPacUrl());
			preferences.put(KSE3_PROXY, ProxyConfigurationType.PAC.name());
		} else if (proxySelector instanceof ManualProxySelector) {
			ManualProxySelector manualProxySelector = (ManualProxySelector) proxySelector;

			ProxyAddress httpProxyAddress = manualProxySelector.getHttpProxyAddress();
			if (httpProxyAddress != null) {
				preferences.put(KSE3_HTTPHOST, httpProxyAddress.getHost());
				preferences.putInt(KSE3_HTTPPORT, httpProxyAddress.getPort());
			}

			ProxyAddress httpsProxyAddress = manualProxySelector.getHttpsProxyAddress();
			if (httpsProxyAddress != null) {
				preferences.put(KSE3_HTTPSHOST, httpsProxyAddress.getHost());
				preferences.putInt(KSE3_HTTPSPORT, httpsProxyAddress.getPort());
			}

			ProxyAddress socksProxyAddress = manualProxySelector.getSocksProxyAddress();
			if (socksProxyAddress != null) {
				preferences.put(KSE3_SOCKSHOST, socksProxyAddress.getHost());
				preferences.putInt(KSE3_SOCKSPORT, socksProxyAddress.getPort());
			}

			preferences.put(KSE3_PROXY, ProxyConfigurationType.MANUAL.name());
		}
	}

	/**
	 * Clear application settings in persistent store.
	 *
	 * @throws BackingStoreException
	 *             If a failure occurred in the backing store
	 */
	public void clear() throws BackingStoreException {
		Preferences preferences = getUnderlyingPreferences();
		preferences.clear();
	}

	private Preferences getUnderlyingPreferences() {
		// Get underlying Java preferences
		return Preferences.userRoot().node(PREFS_NODE);
	}

	/**
	 * Add a new SSL port to start of current list of ports.
	 *
	 * Maximum number is 10. If port is already in list, it is brought to the first position.
	 *
	 * @param newSslPort New SSL port
	 */
	public void addSslPort(String newSslPort) {

		String newSslPorts = StringUtils.addToList(newSslPort, getSslPorts(), 10);

		setSslPorts(newSslPorts);
	}


	/**
	 * Add a new SSL host to start of current list of hosts.
	 *
	 * Maximum number is 10. If host is already in list, it is brought to the first position.
	 *
	 * @param newSslHost New SSL host
	 */
	public void addSslHost(String newSslHost) {

		String newSslHosts = StringUtils.addToList(newSslHost, getSslHosts(), 10);

		setSslHosts(newSslHosts);
	}

	/**
	 * Add a new PKCS#11 library path host to start of current list of libraries.
	 *
	 * Maximum number is 10. If host is already in list, it is brought to the first position.
	 *
	 */
	public void addP11Lib(String p11Lib) {

		String newP11Libs = StringUtils.addToList(p11Lib, getP11Libs(), 10);

		setP11Libs(newP11Libs);
	}



	public boolean getUseCaCertificates() {
		return useCaCertificates;
	}

	public void setUseCaCertificates(boolean useCaCertificates) {
		this.useCaCertificates = useCaCertificates;
	}

	public File getCaCertificatesFile() {
		return caCertificatesFile;
	}

	public void setCaCertificatesFile(File caCertificatesFile) {
		this.caCertificatesFile = caCertificatesFile;
	}

	public boolean getUseWindowsTrustedRootCertificates() {
		return useWindowsTrustedRootCertificates;
	}

	public void setUseWindowsTrustedRootCertificates(boolean useWindowsTrustedRootCertificates) {
		this.useWindowsTrustedRootCertificates = useWindowsTrustedRootCertificates;
	}

	public boolean getEnableImportTrustedCertTrustCheck() {
		return enableImportTrustedCertTrustCheck;
	}

	public void setEnableImportTrustedCertTrustCheck(boolean enableImportTrustedCertTrustCheck) {
		this.enableImportTrustedCertTrustCheck = enableImportTrustedCertTrustCheck;
	}

	public boolean getEnableImportCaReplyTrustCheck() {
		return enableImportCaReplyTrustCheck;
	}

	public void setEnableImportCaReplyTrustCheck(boolean enableImportCaReplyTrustCheck) {
		this.enableImportCaReplyTrustCheck = enableImportCaReplyTrustCheck;
	}

	public KeyPairType getGenerateKeyPairType() {
		return generateKeyPairType;
	}

	public void setGenerateKeyPairType(KeyPairType generateKeyPairType) {
		this.generateKeyPairType = generateKeyPairType;
	}

	public int getGenerateKeyPairSize() {
		return generateKeyPairSize;
	}

	public void setGenerateKeyPairSize(int generateKeyPairSize) {
		this.generateKeyPairSize = generateKeyPairSize;
	}

	public SecretKeyType getGenerateSecretKeyType() {
		return generateSecretKeyType;
	}

	public void setGenerateSecretKeyType(SecretKeyType generateSecretKeyType) {
		this.generateSecretKeyType = generateSecretKeyType;
	}

	public int getGenerateSecretKeySize() {
		return generateSecretKeySize;
	}

	public void setGenerateSecretKeySize(int generateSecretKeySize) {
		this.generateSecretKeySize = generateSecretKeySize;
	}

	public DigestType getCertificateFingerprintType() {
		return certificateFingerprintType;
	}

	public void setCertificateFingerprintType(DigestType certificateFingerprintType) {
		this.certificateFingerprintType = certificateFingerprintType;
	}

	public Rectangle getSizeAndPosition() {
		return sizeAndPosition;
	}

	public void setSizeAndPosition(Rectangle sizeAndPosition) {
		this.sizeAndPosition = sizeAndPosition;
	}

	public boolean getShowToolBar() {
		return showToolBar;
	}

	public void setShowToolBar(boolean showToolBar) {
		this.showToolBar = showToolBar;
	}

	public boolean getShowStatusBar() {
		return showStatusBar;
	}

	public void setShowStatusBar(boolean showStatusBar) {
		this.showStatusBar = showStatusBar;
	}

	public int getTabLayout() {
		return tabLayout;
	}

	public void setTabLayout(int tabLayout) {
		this.tabLayout = tabLayout;
	}

	public File[] getRecentFiles() {
		return recentFiles;
	}

	public void setRecentFiles(File[] recentFiles) {
		this.recentFiles = recentFiles;
	}

	public File getCurrentDirectory() {
		return currentDirectory;
	}

	public void setCurrentDirectory(File currentDirectory) {
		this.currentDirectory = currentDirectory;
	}

	public String getLookAndFeelClass() {
		return lookAndFeelClass;
	}

	public void setLookAndFeelClass(String lookAndFeelClass) {
		this.lookAndFeelClass = lookAndFeelClass;
	}

	public boolean getLookAndFeelDecorated() {
		return lookAndFeelDecorated;
	}

	public void setLookAndFeelDecorated(boolean lookAndFeelDecorated) {
		this.lookAndFeelDecorated = lookAndFeelDecorated;
	}

	public boolean getShowTipsOnStartUp() {
		return showTipsOnStartUp;
	}

	public void setShowTipsOnStartUp(boolean showTipsOnStartUp) {
		this.showTipsOnStartUp = showTipsOnStartUp;
	}

	public int getNextTipIndex() {
		return nextTipIndex;
	}

	public void setNextTipIndex(int nextTipIndex) {
		this.nextTipIndex = nextTipIndex;
	}

	public String getDefaultDN() {
		return defaultDN;
	}

	public void setDefaultDN(String defaultDN) {
		this.defaultDN = defaultDN;
	}

	public String getSslHosts() {
		return sslHosts;
	}

	public void setSslHosts(String sslHosts) {
		this.sslHosts = sslHosts;
	}

	public String getSslPorts() {
		return sslPorts;
	}

	public void setSslPorts(String sslPorts) {
		this.sslPorts = sslPorts;
	}

	public boolean isAutoUpdateCheckEnabled() {
		return autoUpdateCheckEnabled;
	}

	public void setAutoUpdateCheckEnabled(boolean autoUpdateCheckEnabled) {
		this.autoUpdateCheckEnabled = autoUpdateCheckEnabled;
	}

	public Date getAutoUpdateCheckLastCheck() {
		return autoUpdateCheckLastCheck;
	}

	public void setAutoUpdateCheckLastCheck(Date autoUpdateCheckLastCheck) {
		this.autoUpdateCheckLastCheck = autoUpdateCheckLastCheck;
	}

	public int getAutoUpdateCheckInterval() {
		return autoUpdateCheckInterval;
	}

	public void setAutoUpdateCheckInterval(int autoUpdateCheckInterval) {
		this.autoUpdateCheckInterval = autoUpdateCheckInterval;
	}

	public String getP11Libs() {
		return p11Libs;
	}

	public void setP11Libs(String p11Libs) {
		this.p11Libs = p11Libs;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public int getExpiryWarndays() {
		return expiryWarnDays;
	}

	public void setExpiryWarndays(int expiryWarnDays) {
		this.expiryWarnDays = expiryWarnDays;
	}
}
