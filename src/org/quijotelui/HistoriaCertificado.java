package org.quijotelui;

import org.bouncycastle.asn1.ASN1String;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.util.encoders.Hex;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.kse.crypto.CryptoException;
import org.kse.crypto.keystore.KeyStoreUtil;
import org.kse.crypto.x509.X509CertUtil;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HistoriaCertificado {

    private static final int expiryWarnDays = 0;

    public List<Historia> getInformacion(String PKCS12_RESOURCE, String PKCS12_PASSWORD) throws org.kse.crypto.CryptoException {

        List<Historia> historia = new ArrayList();

        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(new FileInputStream(PKCS12_RESOURCE), PKCS12_PASSWORD.toCharArray());

            Enumeration<String> aliases = keyStore.aliases();
            TreeMap<String, String> sortedAliases = new TreeMap<>(new AliasComparator());

            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                if (!KeyStoreUtil.isSupportedEntryType(alias, keyStore)) {
                    continue;
                }
                sortedAliases.put(alias, alias);
            }
            Date expiry = new Date();
            int daysExpiry = 0;
            String ownerCert = "";
            String issuerCert = "";

            for (Iterator<Map.Entry<String, String>> itr = sortedAliases.entrySet().iterator(); itr.hasNext();) {
                String alias = itr.next().getKey();
//                System.out.println("Alias: " + alias);

                // Expiry status column
                expiry = getCertificateExpiry(alias, keyStore);
                Date dateNow = new Date();
                daysExpiry = Days.daysBetween(new DateTime(dateNow), new DateTime(expiry)).getDays();
                ownerCert = alias.substring(alias.lastIndexOf("cn=") + 3, alias.lastIndexOf(",l=")).toUpperCase();
                issuerCert = alias.substring(alias.lastIndexOf("o=") + 2, alias.lastIndexOf(",c=")).toUpperCase();

//                System.out.println("CertificateSubjectCN: " + getCertificateSubjectCN(alias, keyStore));
//                System.out.println("CertificateIssuerCN:" + getCertificateIssuerCN(alias, keyStore));
//                System.out.println("CertificateIssuerDN: " + getCertificateIssuerDN(alias, keyStore));
//                System.out.println("CertificateSubjectDN: " + getCertificateSubjectDN(alias, keyStore));
//                System.out.println("CertificateAKI: " + getCertificateAKI(alias, keyStore));
//                System.out.println("CertificateSKI: " + getCertificateSKI(alias, keyStore));
//                System.out.println("CertificateSubjectO: " + getCertificateSubjectO(alias, keyStore));
//                System.out.println("CertificateIssuerO: " + getCertificateIssuerO(alias, keyStore));
            }
            SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

            historia.add(new Historia(
                    ownerCert,
                    issuerCert,
                    DATE_FORMAT.format(expiry),
                    Integer.toString(daysExpiry)));

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException ex) {
            Logger.getLogger(HistoriaCertificado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return historia;
    }

    private Date getCertificateExpiry(String alias, KeyStore keyStore) throws KeyStoreException, org.kse.crypto.CryptoException {
        if (KeyStoreUtil.isTrustedCertificateEntry(alias, keyStore)) {
            return X509CertUtil.convertCertificate(keyStore.getCertificate(alias)).getNotAfter();
        } else {
            Certificate[] chain = keyStore.getCertificateChain(alias);

            if (chain == null) {
                // Key entry - no expiry date
                return null;
            }

            // Key pair - first certificate in chain will be for the private key
            X509Certificate[] x509Chain = X509CertUtil.orderX509CertChain(X509CertUtil.convertCertificates(chain));
            if (expiryWarnDays < 1) {
                return x509Chain[0].getNotAfter();
            } else {
                Calendar cal = Calendar.getInstance();
                cal.set(9999, 1, 1);
                Date earliest = cal.getTime();
                for (int i = 0; i < x509Chain.length; i++) {
                    if (x509Chain[i].getNotAfter().before(earliest)) {
                        earliest = x509Chain[i].getNotAfter();
                    }
                }
                return earliest;
            }
        }
    }

    private String getCertificateSubjectDN(String alias, KeyStore keyStore) throws CryptoException, KeyStoreException {
        if (KeyStoreUtil.isTrustedCertificateEntry(alias, keyStore)) {
            return X509CertUtil.convertCertificate(keyStore.getCertificate(alias)).getSubjectDN().getName();
        } else {
            Certificate[] chain = keyStore.getCertificateChain(alias);
            if (chain == null) {
                return null;
            }
            // Key pair - first certificate in chain will be for the private key
            X509Certificate[] x509Chain = X509CertUtil.orderX509CertChain(X509CertUtil.convertCertificates(chain));
            return x509Chain[0].getSubjectDN().getName();
        }
    }
    private String getCertificateIssuerDN(String alias, KeyStore keyStore) throws CryptoException, KeyStoreException {
        if (KeyStoreUtil.isTrustedCertificateEntry(alias, keyStore)) {
            return X509CertUtil.convertCertificate(keyStore.getCertificate(alias)).getIssuerDN().getName();
        } else {
            Certificate[] chain = keyStore.getCertificateChain(alias);
            if (chain == null) {
                return null;
            }
            // Key pair - first certificate in chain will be for the private key
            X509Certificate[] x509Chain = X509CertUtil.orderX509CertChain(X509CertUtil.convertCertificates(chain));
            return x509Chain[0].getIssuerDN().getName();
        }
    }
    private String getCertificateSubjectCN(String alias, KeyStore keyStore) throws CryptoException, KeyStoreException {
        X509Certificate x509Cert = null;
        if (KeyStoreUtil.isTrustedCertificateEntry(alias, keyStore)) {
            x509Cert= X509CertUtil.convertCertificate(keyStore.getCertificate(alias));
        } else {
            Certificate[] chain = keyStore.getCertificateChain(alias);
            if (chain == null) {
                return null;
            }
            // Key pair - first certificate in chain will be for the private key
            X509Certificate[] x509Chain = X509CertUtil.orderX509CertChain(X509CertUtil.convertCertificates(chain));
            x509Cert = x509Chain[0];
        }
        X500Name subject;
        try {
            subject = new JcaX509CertificateHolder(x509Cert).getSubject();
            RDN cn = subject.getRDNs(BCStyle.CN)[0];
            return ((ASN1String) cn.getFirst().getValue()).getString();
        } catch (Exception e) {
            return "";
        }
    }

    private String getCertificateIssuerCN(String alias, KeyStore keyStore) throws CryptoException, KeyStoreException {
        X509Certificate x509Cert = null;
        if (KeyStoreUtil.isTrustedCertificateEntry(alias, keyStore)) {
            x509Cert= X509CertUtil.convertCertificate(keyStore.getCertificate(alias));
        } else {
            Certificate[] chain = keyStore.getCertificateChain(alias);
            if (chain == null) {
                return null;
            }
            // Key pair - first certificate in chain will be for the private key
            X509Certificate[] x509Chain = X509CertUtil.orderX509CertChain(X509CertUtil.convertCertificates(chain));
            x509Cert = x509Chain[0];
        }
        try {
            RDN cn = new JcaX509CertificateHolder(x509Cert).getIssuer().getRDNs(BCStyle.CN)[0];
            return ((ASN1String) cn.getFirst().getValue()).getString();
        } catch (Exception e) {
            return "";
        }
    }

    private String getCertificateAKI(String alias, KeyStore keyStore) throws CryptoException, KeyStoreException {
        X509Certificate x509Cert = null;
        if (KeyStoreUtil.isTrustedCertificateEntry(alias, keyStore)) {
            x509Cert= X509CertUtil.convertCertificate(keyStore.getCertificate(alias));
        } else {
            Certificate[] chain = keyStore.getCertificateChain(alias);
            if (chain == null) {
                return null;
            }
            // Key pair - first certificate in chain will be for the private key
            X509Certificate[] x509Chain = X509CertUtil.orderX509CertChain(X509CertUtil.convertCertificates(chain));
            x509Cert = x509Chain[0];
        }
        try {
            String aki = Hex.toHexString(x509Cert.getExtensionValue("2.5.29.35"));
            return aki.substring(12); // remove object header 041830168014
        } catch (Exception e) {
            return "-";
        }
    }

    private String getCertificateSKI(String alias, KeyStore keyStore) throws CryptoException, KeyStoreException {
        X509Certificate x509Cert = null;
        if (KeyStoreUtil.isTrustedCertificateEntry(alias, keyStore)) {
            x509Cert= X509CertUtil.convertCertificate(keyStore.getCertificate(alias));
        } else {
            Certificate[] chain = keyStore.getCertificateChain(alias);
            if (chain == null) {
                return null;
            }
            // Key pair - first certificate in chain will be for the private key
            X509Certificate[] x509Chain = X509CertUtil.orderX509CertChain(X509CertUtil.convertCertificates(chain));
            x509Cert = x509Chain[0];
        }
        try {
            String ski = Hex.toHexString(x509Cert.getExtensionValue("2.5.29.14"));
            return ski.substring(8); // remove object header 04160414
        } catch (Exception e) {
            return "-";
        }
    }

    private String getCertificateSubjectO(String alias, KeyStore keyStore) throws CryptoException, KeyStoreException {
        X509Certificate x509Cert = null;
        if (KeyStoreUtil.isTrustedCertificateEntry(alias, keyStore)) {
            x509Cert= X509CertUtil.convertCertificate(keyStore.getCertificate(alias));
        } else {
            Certificate[] chain = keyStore.getCertificateChain(alias);
            if (chain == null) {
                return null;
            }
            // Key pair - first certificate in chain will be for the private key
            X509Certificate[] x509Chain = X509CertUtil.orderX509CertChain(X509CertUtil.convertCertificates(chain));
            x509Cert = x509Chain[0];
        }
        X500Name subject;
        try {
            subject = new JcaX509CertificateHolder(x509Cert).getSubject();
            RDN cn = subject.getRDNs(BCStyle.O)[0];
            if (cn.size()>0) {
                return ((ASN1String) cn.getFirst().getValue()).getString();
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    private String getCertificateIssuerO(String alias, KeyStore keyStore) throws CryptoException, KeyStoreException {
        X509Certificate x509Cert = null;
        if (KeyStoreUtil.isTrustedCertificateEntry(alias, keyStore)) {
            x509Cert= X509CertUtil.convertCertificate(keyStore.getCertificate(alias));
        } else {
            Certificate[] chain = keyStore.getCertificateChain(alias);
            if (chain == null) {
                return null;
            }
            // Key pair - first certificate in chain will be for the private key
            X509Certificate[] x509Chain = X509CertUtil.orderX509CertChain(X509CertUtil.convertCertificates(chain));
            x509Cert = x509Chain[0];
        }
        try {
            RDN cn = new JcaX509CertificateHolder(x509Cert).getIssuer().getRDNs(BCStyle.O)[0];
            if (cn.size()>0) {
                return ((ASN1String) cn.getFirst().getValue()).getString();
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    private class AliasComparator implements Comparator<String> {

        @Override
        public int compare(String name1, String name2) {
            return name1.compareToIgnoreCase(name2);
        }
    }
}
