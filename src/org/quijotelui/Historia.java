package org.quijotelui;

public class Historia {
    private String ownerCert;
    private String issuerCert;
    private String dateExpiry;
    private String formatDate;
    private String daysToExpiry;

    public Historia(String ownerCert, String issuerCert, String dateExpiry, String daysToExpiry) {
        this.ownerCert = ownerCert;
        this.issuerCert = issuerCert;
        this.dateExpiry = dateExpiry;
        this.daysToExpiry = daysToExpiry;
        this.formatDate = "aaaa-mm-dd";
    }

    public String getOwnerCert() {
        return ownerCert;
    }

    public void setOwnerCert(String ownerCert) {
        this.ownerCert = ownerCert;
    }

    public String getIssuerCert() {
        return issuerCert;
    }

    public void setIssuerCert(String issuerCert) {
        this.issuerCert = issuerCert;
    }

    public String getDateExpiry() {
        return dateExpiry;
    }

    public void setDateExpiry(String dateExpiry) {
        this.dateExpiry = dateExpiry;
    }

    public String getDaysToExpiry() {
        return daysToExpiry;
    }

    public void setDaysToExpiry(String daysToExpiry) {
        this.daysToExpiry = daysToExpiry;
    }

    public String getFormatDate() {
        return formatDate;
    }
}
