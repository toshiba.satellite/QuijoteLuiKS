# QuijoteLuiKS

Retorna la información del certificado digital

## Compilar
```
$ gradle build
```
## Añadir al repositorio Maven Local
```
$ mvn install:install-file -Dfile=./build/libs/QuijoteLuiKS-1.1.jar -DgroupId=org.quijotelui -DartifactId=QuijoteLuiKS -Dversion=1.1 -Dpackaging=jar
```